import { NextRouter } from "next/router";

export interface WithRouterProps {
  router: NextRouter;
}
export interface IField {
  fieldType:
    | "checkmark"
    | "date"
    | "dropdown"
    | "file"
    | "form"
    | "group"
    | "input"
    | "label"
    | "number"
    | "password"
    | "radioButton";
  fieldID: string;
  fields?: IField[];
  fieldTitle: string;
  required?: boolean;
}

export interface PhoneNumbers {
  cell?: number;
  home?: number;
  office?: number;
}

export interface Name {
  firstName: string;
  middleName: string;
  lastName: string;
}

export interface Address {
  city: string;
  country: string;
  postalCode: string;
  state: string;
  streetName: string;
  streetNameTwo: string;
  addressType: string;
}

export interface Contact {
  relationship: string;
  name: Name;
  address: Address;
  gender: string;
}

export interface IPatient {
  _id: string;
  address: Address;
  birthday: Date;
  contact: Contact;
  description: string;
  emailAdddress: string;
  firstName: string;
  forDoctorId: string;
  gender: "Male" | "Female"; // TODO: Add more genders later on.
  height: number;
  heightType: string;
  lastName: string;
  phoneNumbers: PhoneNumbers;
  sexuality: string;
  weight: number;
  weightType: string;
}
