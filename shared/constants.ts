import { faUserMd, faMedkit, faBook } from "@fortawesome/free-solid-svg-icons";

export const JWT_TOKEN_CONST = "jwt-token";
export const USER_EMAIL_CONST = "userEmail";

export const homePageCardData = [
  {
    cardTitle: "Connect with your Doctor",
    description:
      "Find in-depth information about your visit, directly from the Doctor.",
    hrLine: true,
    icon: faUserMd,
  },
  {
    cardTitle: "Look at all your health data",
    description:
      "Easily access all the necesssary data as a Doctor or a patient.",
    hrLine: true,
    icon: faMedkit,
  },
  {
    cardTitle: "Get assisted by AI",
    description:
      "Talk to TiHealth AI to find out in-depth smartly curated information about your symptoms, medications, and much more.",
    hrLine: true,
    icon: faBook,
  },
  {
    cardTitle: "Upload or Download Healthcare Documents",
    description:
      "Easily upload important documents related to your health and/or download documents uploaded by your doctor.",
    hrLine: true,
    icon: faBook,
  },
];
