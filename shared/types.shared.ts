export type GetProfileResponseType = {
  __v?: number;
  _id: string;
  firstName?: string;
  lastName?: string;
  roles?: string[];
  userBirthday?: Date;
  userEmail?: string;
  iat?: number;
  exp?: number;
};

export type UserFilesUploadType = {
  __v: number;
  _id: string;
  description: string;
  file: any[];
  for: string;
  name: string;
  uploadBy: string;
  uploadTime: Date;
};

export type UserFilesResponseType = {
  error: boolean;
  resWithFiles: UserFilesUploadType[];
  id: string;
  length: number;
  message: string;
};
