import { IField } from "./interfaces.shared";
import CustomDropdown from "../components/dropdown/Dropdown.component";
import CustomInput from "../components/custom_input/custom_input.component";
import CustomRadio from "../components/custom-radio/CustomRadio.component";
import styles from "../styles/Start.module.scss";

const emailAddressRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

/**
 * Create the registration form.
 * @param field IField
 * @param customId string (Optional)
 * @returns
 */
export const getFormFields = (
  customId: string = undefined,
  field: IField,
  formId: string
) => {
  let formField = [];
  switch (field.fieldType) {
    case "checkmark":
      formField.push(
        <div className={`form-check ${styles.form_field}`} key={field.fieldID}>
          <input
            className="form-check-input"
            type="checkbox"
            value=""
            id="flexCheckDefault"
            form={formId}
          />
          <label className="form-check-label" htmlFor="flexCheckDefault">
            {field.fieldTitle}
          </label>
        </div>
      );
      break;
    case "date":
      formField.push(
        <div className={`form-check ${styles.form_field}`} key={field.fieldID}>
          <label className="form-check-label">{field.fieldTitle}</label>
          <input
            className="form-control"
            type="date"
            id={field.fieldID}
            form={formId}
            required
          />
        </div>
      );
      break;
    case "label":
      return (
        <div className={`form-check ${styles.form_field}`} key={field.fieldID}>
          <span id={field.fieldID}>{field.fieldTitle}</span>
        </div>
      );
    case "dropdown":
      const tempDropdown = {
        ...field,
        formId,
      };
      formField.push(
        <div className={`form-check ${styles.form_field}`} key={field.fieldID}>
          <CustomDropdown {...tempDropdown} />
        </div>
      );
      break;
    case "file":
      break;
    case "form":
      break;
    case "input":
    case "number":
    case "password":
      const tempInput = {
        ...field,
        formId,
      };
      formField.push(
        <div className={`form-check ${styles.form_field}`} key={field.fieldID}>
          <CustomInput {...tempInput} />
        </div>
      );
      break;
    case "radioButton":
      const tempField = {
        ...field,
        customId,
        formId,
      };
      formField.push(
        <div className={`form-check ${styles.form_field}`} key={field.fieldID}>
          <CustomRadio {...tempField} />
        </div>
      );
      break;
    case "group":
      formField.push(
        <>
          <hr />
          <div
            className={`form-check ${styles.form_field_group}`}
            key={field.fieldID}
          >
            <h4 className="form-check-label" id={field.fieldID}>
              {field.fieldTitle}
            </h4>
            {field.fields.map((eachField: IField) => {
              return getFormFields(field.fieldID, eachField, formId)[0];
            })}
          </div>
        </>
      );
      break;
    default:
      break;
  }

  return formField;
};

export const validateEmailAddress = (e: any): boolean => {
  if (e.target && e.target.id && e.target.id.toLowerCase().includes("email")) {
    return emailAddressRegex.test(String(e.target.value).toLowerCase());
  }
  return true;
};

/**
 * Validate first page registration form
 */
export const validateField = (id: string, value: any) => {
  switch (id) {
    case "userEmail":
      return emailAddressRegex.test(String(value).toLowerCase());
    default:
      return true;
  }
};
