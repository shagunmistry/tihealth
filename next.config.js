const {
  PHASE_DEVELOPMENT_SERVER,
  PHASE_PRODUCTION_BUILD,
} = require("next/constants");

module.exports = (phase) => {
  // when started in development mode `next dev` or `npm run dev` regardless of the value of STAGING environmental variable
  const isDev = phase === PHASE_DEVELOPMENT_SERVER;
  // when `next build` or `npm run build` is used
  const isProd = phase === PHASE_PRODUCTION_BUILD;

  return {
    env: {
      TIHEALTH_API_URL: isDev
        ? "http://localhost:5000/"
        : "https://tihealth-api-func.azurewebsites.net/api/",
    },
    images: {
      domains: ["firebasestorage.googleapis.com"],
    },
  };
};
