import Cookies from "cookies";
import jsCookie from "js-cookie";
import { JWT_TOKEN_CONST, USER_EMAIL_CONST } from "../shared/constants";

/**
 * Get auth related cookies
 * @param cookies
 * @returns
 */
export const getAuthCookies = (cookies: Cookies) => {
  const userToken = cookies.get(JWT_TOKEN_CONST) || null;
  const userEmail = cookies.get(USER_EMAIL_CONST) || null;

  return {
    userToken,
    userEmail,
  };
};

export const resetAuthCookies = () => {
  try {
    jsCookie.remove(JWT_TOKEN_CONST);
    jsCookie.remove(USER_EMAIL_CONST);
  } catch (e: any) {
    console.log("ERROR -> Error resetting auth Cookies ");
  }
};

export const logUserOut = () => {};

/**
 * Convert file buffer into Uint8Array
 * @param base64 file buffer string
 * @returns
 */
export const convertBufferIntoUInt8Array = (base64: string): Uint8Array => {
  const binaryString = window.atob(base64);
  const binaryLen = binaryString.length;
  const bytes = new Uint8Array(binaryLen);
  for (let i = 0; i < binaryLen; i++) {
    const ascii = binaryString.charCodeAt(i);
    bytes[i] = ascii;
  }
  return bytes;
};
