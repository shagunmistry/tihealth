import axios from "axios";

import { GetProfileResponseType } from "../shared/types.shared";

export const isUserLoggedIn = async (
  token: string,
  userEmail: string
): Promise<GetProfileResponseType> => {
  /**
   * Check if the user is valid/authenticated
   * 1. Get JWT Token from http-only cookie
   * 2. Send token to API
   * 3. Redirect based on response.
   */

  try {
    const isLoggedIn = await axios.get(
      `${process.env.TIHEALTH_API_URL}auth/profile/${userEmail}`,
      {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      }
    );

    if (isLoggedIn && isLoggedIn.status >= 200 && isLoggedIn.status < 400) {
      return Promise.resolve(isLoggedIn.data);
    } else {
      throw new Error(`There was an error: ${isLoggedIn.status} `);
    }
  } catch (e: any) {
    console.log("IsLoggedIn Error: ", JSON.stringify(e.message));
    return Promise.resolve({
      _id: null,
    });
  }
};
