import { useEffect, useState } from "react";
import { convertBufferIntoUInt8Array } from "../../middleware/utils";
import { UserFilesUploadType } from "../../shared/types.shared";
import styles from "./Image-gallery.module.scss";

interface MyComponentProps {
  error: boolean;
  resWithFiles: UserFilesUploadType[];
  id: string;
  length: number;
  message: string;
  patients: Promise<Map<string, string>>;
}

const ImageGallery = (props: MyComponentProps, state: {}) => {
  const [patientsMap, setPatientsMap] = useState(new Map<string, string>());

  useEffect(() => {
    props.patients.then((val: Map<string, string>) => setPatientsMap(val));
  });

  /**
   * Converts the byte array into a blob and saves it for the user.
   * @param reportName file name
   * @param byte File buffer array
   * @param fileType file type
   */
  const saveByteArray = (
    reportName: string,
    byte: Uint8Array,
    fileType: string
  ) => {
    console.log("original name: ", reportName);
    const blob = new Blob([byte], { type: fileType });
    const link = document.createElement("a");
    link.href = window.URL.createObjectURL(blob);
    link.download = reportName;
    link.click();
    link.remove();
  };

  const getFileDownload = (data: any) => {
    const bytes = convertBufferIntoUInt8Array(data.buffer);
    saveByteArray(data.originalname, bytes, data.mimetype);
  };

  const getPatientName = (id: string): string => {
    return patientsMap.get(id);
  };

  return (
    <div className="container">
      <div className="card-columns">
        {props.length > 0 &&
          props.resWithFiles.length > 0 &&
          props.resWithFiles.map((each: UserFilesUploadType, idx) => {
            return (
              <div
                className={`card ${styles.documentsCard_Body_ListGroup_Item}`}
                key={`${each.file[0].originalname}-${idx}`}
                style={{ color: "black" }}
              >
                <div className="card-body">
                  <h5 className="card-title">
                    {each?.name || each.file[0].originalname}
                  </h5>
                  <h6 className="card-subtitle mb-2 text-muted">
                    For: {getPatientName(each.for)}
                  </h6>
                  <ul className="list-group list-group-flush">
                    <li className="list-group-item">{each.description}</li>
                    <li className="list-group-item">
                      Uploaded: {new Date(each.uploadTime).toDateString()}
                    </li>
                  </ul>
                </div>
                <button
                  key={each._id}
                  type="button"
                  className={`btn btn-primary`}
                  onClick={() => getFileDownload(each.file[0])}
                >
                  Download
                </button>
              </div>
            );
          })}
      </div>
    </div>
  );
};
export default ImageGallery;
