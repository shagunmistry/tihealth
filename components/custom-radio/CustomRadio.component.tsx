import { IField } from "../../shared/interfaces.shared";

interface RadioProps extends IField {
  customId?: string;
  formId?: string;
}

const CustomRadio = (field: RadioProps) => {
  return (
    <>
      <div className="form-check">
        <input
          className="form-check-input"
          type="radio"
          name={field.customId || field.fieldID}
          id={field.fieldID}
          value={field.fieldID}
        />
        <label className="form-check-label" htmlFor={field.fieldID}>
          {field.fieldTitle}
        </label>
      </div>
    </>
  );
};

export default CustomRadio;
