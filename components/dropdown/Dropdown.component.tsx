import { useState } from "react";
import { getFormFields } from "../../shared/helper-methods.shared";
import { IField } from "../../shared/interfaces.shared";

import styles from "./dropdown.module.scss";

interface DropdownProps extends IField {
  customId?: string;
  formId?: string;
}
// TODO: Add dropdown items
const CustomDropdown = (props: DropdownProps) => {
  const [fieldTitle, setFieldTitle] = useState(props.fieldTitle);

  const getDropdownSelectValue = (id: string) => {
    if (id !== "") {
      const val = (document.getElementById(id) as HTMLInputElement).innerText;
      setFieldTitle(val);
    }
  };
  return (
    <div
      className="dropdown"
      onClick={(e: any) => getDropdownSelectValue(e.target.id)}
    >
      <button
        className="btn btn-primary dropdown-toggle btn-sm"
        type="button"
        id={props.customId || props.fieldID}
        data-bs-toggle="dropdown"
        aria-expanded="false"
      >
        {fieldTitle}
      </button>
      <div
        className="dropdown-menu"
        aria-labelledby={props.customId || props.fieldID}
      >
        {props.fields &&
          props.fields.map((eachItem) => {
            return (
              <li
                className={`dropdown-item ${styles.dropdownItem}`}
                key={eachItem.fieldID}
                id={eachItem.fieldID}
                value={eachItem.fieldTitle}
              >
                {getFormFields(undefined, eachItem, props.formId)}{" "}
              </li>
            );
          })}
      </div>
    </div>
    // <div className="dropdown">
    //   <button
    //     className="btn btn-primary dropdown-toggle"
    //     type="button"
    //     id={`${props.fieldID}-select`}
    //     data-bs-toggle="dropdown"
    //     aria-expanded="false"
    //     form={props.formId}
    //   >
    //     {props.fieldTitle}
    //   </button>
    //   <ul
    //     className="dropdown-menu"
    //     aria-label={`${props.fieldID}-select`}
    //     id={props.fieldID}
    //     aria-labelledby={`${props.fieldID}-select`}
    //   >
    //     <li value={props.fieldTitle}>Hello World</li>
    //
    //   </ul>
    // </div>
  );
};

export default CustomDropdown;
