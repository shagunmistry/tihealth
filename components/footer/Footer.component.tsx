import { faHeartbeat } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import styles from "../../styles/Footer.module.scss";

const CustomFooter = ({}) => {
  return (
    <footer className={styles.footer}>
      <FontAwesomeIcon className={styles.footer_icon} icon={faHeartbeat} />
      @2021 TiHealth
    </footer>
  );
};

export default CustomFooter;
