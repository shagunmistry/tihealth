import { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowDown } from "@fortawesome/free-solid-svg-icons";

import styles from "./DocumentsCard.module.scss";
import CustomLoader from "../custom-loader/Custom-loader.component";
import { UserFilesUploadType } from "../../shared/types.shared";
import { convertBufferIntoUInt8Array } from "../../middleware/utils";

type MyComponentProps = {
  error: boolean;
  id: string;
  length: number;
  loading: boolean;
  message: string;
  patients: Promise<Map<string, string>>;
  resWithFiles: UserFilesUploadType[];
  uploadFiles: Function;
};

type MyComponentState = {
  patients: Object[];
};
class DocumentsCard extends Component<MyComponentProps, MyComponentState> {
  componentDidMount() {
    this.getPatientsListForDropdown();
  }
  /**
   * Pass the files to Upload files method in the upper component.
   */
  handleFileUpload = () => {
    const el = document.getElementById(
      "validatedCustomFile"
    ) as HTMLInputElement;

    const description = (
      document.getElementById("fileDescription") as HTMLInputElement
    )?.value;
    const fileName = (document.getElementById("fileName") as HTMLInputElement)
      ?.value;
    const selectedPatient = (
      document.getElementById("patient-selection") as HTMLInputElement
    )?.value;

    if (el.files && el.files.length > 0 && selectedPatient !== "") {
      const body = {
        description: description || "",
        for: selectedPatient,
        name: fileName || el.files[0].name,
        uploadTime: new Date(),
        file: el.files[0],
      };
      this.props.uploadFiles(body);
    }
  };

  /**
   * Converts the byte array into a blob and saves it for the user.
   * @param reportName file name
   * @param byte File buffer array
   * @param fileType file type
   */
  saveByteArray = (reportName: string, byte: Uint8Array, fileType: string) => {
    console.log("original name: ", reportName);
    const blob = new Blob([byte], { type: fileType });
    const link = document.createElement("a");
    link.href = window.URL.createObjectURL(blob);
    link.download = reportName;
    link.click();
    link.remove();
  };

  getFileDownload = (data: any) => {
    const bytes = convertBufferIntoUInt8Array(data.buffer);
    this.saveByteArray(data.originalname, bytes, data.mimetype);
  };

  getPatientsListForDropdown = () => {
    let selectOptions: Object[] = [];
    this.props.patients.then((value: Map<string, string>) => {
      console.log("map: ", value);
      selectOptions = [
        ...Array.from(value.entries())
          .filter((val) => !val[1].includes("undefined"))
          .map((patient: [string, string]) => {
            return (
              <option value={patient[0]} key={patient[0]}>
                {patient[1]}
              </option>
            );
          }),
      ];

      console.log("Select options: ", selectOptions);
      this.setState({ patients: selectOptions });
      console.log("Patients: ", this.state.patients);
    });
  };

  render() {
    return (
      <div className={`card ${styles.documentsCard}`}>
        {this.props.loading ? (
          <CustomLoader />
        ) : (
          <>
            <div
              className={`card-header ${styles.documentsCard_Header}`}
              data-bs-toggle="collapse"
              data-bs-target="#documentCardCollapse"
              aria-expanded="false"
              aria-controls="documentCardCollapse"
            >
              <span>Documents</span>
              <FontAwesomeIcon
                className={styles.documentsCard_Header_icon}
                icon={faArrowDown}
              />
            </div>
            <div
              className={`collapse card-body ${styles.documentsCard_Body}`}
              id="documentCardCollapse"
            >
              <small>File Name</small>
              <input
                type="text"
                className={`form-control ${styles.documentsCard_Body_formInput}`}
                id="fileName"
                placeholder="File name..."
              />

              <small>File Description</small>
              <input
                type="text"
                className={`form-control ${styles.documentsCard_Body_formInput}`}
                id="fileDescription"
                placeholder="Enter a short file description"
              />

              <small>Choose the patient</small>
              <select
                className={`custom-select custom-select-lg mb-3 ${styles.documentsCard_Body_formInput}`}
                id="patient-selection"
              >
                {this.state?.patients ? (
                  this.state?.patients.map((val: Object) => val)
                ) : (
                  <option value="0">No patients yet.</option>
                )}
              </select>
              <div className="custom-file">
                <input
                  type="file"
                  className={`custom-file-input ${styles.documentsCard_FileInput}`}
                  id="validatedCustomFile"
                  onChange={() => this.handleFileUpload()}
                />
                <label
                  className="custom-file-label"
                  htmlFor="validatedCustomFile"
                >
                  Choose file(s)...
                </label>
              </div>
            </div>
          </>
        )}
      </div>
    );
  }
}

export default DocumentsCard;
