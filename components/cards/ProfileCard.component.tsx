import { faArrowDown } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { Component } from "react";

import styles from "./ProfileCard.module.scss";

interface ProfileCardProps {
  FirstName: string;
  id: string;
  LastName: string;
  roleType: string;
  userBirthday: Date;
  userEmail: string;
}

class ProfileCard extends Component<ProfileCardProps> {
  render() {
    const { FirstName, id, LastName, roleType, userBirthday, userEmail } =
      this.props;

    return (
      <div className={`card ${styles.profileCard}`}>
        <div
          className={`card-header ${styles.profileCardHeader} ${styles.profileDropdownButton}`}
          data-bs-toggle="collapse"
          data-bs-target="#profileCardCollapse"
          aria-expanded="false"
          aria-controls="profileCardCollapse"
        >
          <span>Profile Info</span>
          <FontAwesomeIcon
            className={styles.profileCardHeader_icon}
            icon={faArrowDown}
          />
        </div>
        <div className="collapse card-body" id="profileCardCollapse">
          <ul
            className={`list-group list-group-flush ${styles.profileListGroup}`}
          >
            <li className={`list-group-item ${styles.profileListGroupItem}`}>
              <label>Birthday:</label>{" "}
              <h5>{userBirthday.toLocaleDateString()}</h5>
            </li>
            <li className={`list-group-item ${styles.profileListGroupItem}`}>
              <label>Email:</label> <h5>{userEmail}</h5>
            </li>
            <li className={`list-group-item ${styles.profileListGroupItem}`}>
              <label>User Type:</label> <h5>{roleType}</h5>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

export default ProfileCard;
