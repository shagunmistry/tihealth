import { Component } from "react";
import { faArrowDown, faPlusSquare } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";

import styles from "./PatientsCard.module.scss";
import CustomCard from "../../components/cards/card.component";
import { IField, IPatient } from "../../shared/interfaces.shared";
import { validateField } from "../../shared/helper-methods.shared";

interface PatientsCardProps {
  doctorId: string;
}
interface PatientCardState {
  addPatientForm: {
    fields?: IField[];
    formId: string;
    title: string;
  };
  errorMessage: string;
  loadingAddPatientForm: boolean;
  patientFormOpen: boolean;
  patientList: IPatient[];
}

class PatientsCard extends Component<PatientsCardProps, PatientCardState> {
  constructor(props: PatientsCardProps) {
    super(props);
    this.state = {
      addPatientForm: null,
      errorMessage: "",
      loadingAddPatientForm: false,
      patientFormOpen: false,
      patientList: [],
    };
  }

  componentDidMount() {
    this.getPatientsList();
  }

  getPatientRegistrationForm = async () => {
    if (!this.state.patientFormOpen) {
      const res = await axios.get(
        `${process.env.TIHEALTH_API_URL}registration/form/2?userType=patient`
      );

      if (res.status === 200) {
        this.setState({ patientFormOpen: true });
        this.setState({ addPatientForm: res.data });
      }
    }
    this.setState({ patientFormOpen: false });
  };

  async getPatientsList() {
    try {
      const res = await axios.get(
        `api/patient?doctorId=${this.props.doctorId}`
      );
      if (res && res.data && res.status === 200) {
        this.setState({ patientList: res.data });
      }
    } catch (e: any) {
      console.log("There was an error getting patient list: ", e);
    }
  }

  render() {
    /**
     * Submit Add Patient Form
     * @param e
     */
    const submitForm = async (e: any) => {
      e.preventDefault();

      let submission: any = {};
      this.setState({ loadingAddPatientForm: true });
      if (e.target && e.target.length > 0) {
        Array.from(e.target).forEach((target: any) => {
          if (target && target.id && target.id !== "") {
            const isValidate = validateField(target.id, target.value);

            // TODO: Fix values for userHomeNumber, userOfficeNumber, and userWeight
            if (isValidate) {
              if (target.checked) {
                // This is for the radio button for deciding user type.
                submission.userType = target.value;
              }
              if (target.className?.includes("dropdown")) {
                submission[target.id] = target.textContent;
              } else {
                submission[target.id] = target.value;
              }
            }
          }
        });

        submission["forDoctorId"] = this.props.doctorId;

        try {
          const res = await axios.post(`api/patient`, submission);
          if (res.status === 200) {
            //TODO: Show success message here
            console.log("RESULT FROM ADDING PATIENT: ", res.data);
            this.getPatientsList();
          }
          this.setState({
            loadingAddPatientForm: false,
            addPatientForm: null,
          });
        } catch (e: any) {
          this.setState({
            loadingAddPatientForm: false,
            errorMessage: `There was an error registering this patient: ${e.message}`,
          });
          console.log("E: ", e.status);
        }
      } else {
        this.setState({ loadingAddPatientForm: false });
      }

      console.log("Submission Obj: ", submission);
    };

    return (
      <div className={`accordion accordion-flush ${styles.patientsCard}`}>
        <div className="accordion-item">
          <h2 className="accordion-header" id="flush-headingOne">
            <button
              aria-controls="addPatientAccordion"
              aria-expanded="false"
              className={`btn btn-outline-primary accordion-button collapsed`}
              data-bs-target="#addPatientAccordion"
              data-bs-toggle="collapse"
              onClick={() => this.getPatientRegistrationForm()}
              type="button"
            >
              Add Patient
              <FontAwesomeIcon
                icon={faPlusSquare}
                className={`${styles.patientsCardButtonIcon}`}
              />
            </button>
          </h2>
          <div
            id="addPatientAccordion"
            className="accordion-collapse collapse"
            aria-labelledby="flush-headingOne"
            data-bs-parent="#addPatientAccordion"
          >
            {this.state.addPatientForm && (
              <CustomCard
                cardTitle={this.state.addPatientForm.title}
                hrLine={false}
                formBody={{
                  fields: this.state.addPatientForm.fields,
                  formId: this.state.addPatientForm.formId,
                  imageAttr: "Login Illustration Pic",
                  imageSrc:
                    "https://firebasestorage.googleapis.com/v0/b/queen-x.appspot.com/o/TiHealth%2Fhealth-illustrations_svg%2Fremote-work-man.svg?alt=media&token=be18724a-525d-40c3-8214-ce42848baefb",
                  submitForm,
                }}
                errorMessage={this.state.errorMessage}
                isForm={true}
                loading={this.state.loadingAddPatientForm}
                showCardImage={false}
              />
            )}
          </div>
        </div>

        {this.state.patientList && this.state.patientList.length > 0 ? (
          <div className="accordion-item">
            <h2 className="accordion-header" id="flush-headingTwo">
              <button
                className={`btn btn-outline-primary accordion-button collapsed`}
                type="button"
                data-bs-toggle="collapse"
                data-bs-target="#patientsListFlush"
                aria-expanded="false"
                aria-controls="patientsListFlush"
              >
                Patients List
                <FontAwesomeIcon
                  icon={faArrowDown}
                  className={`${styles.patientsCardButtonIcon}`}
                />
              </button>
            </h2>
            <div
              id="patientsListFlush"
              className="accordion-collapse collapse"
              aria-labelledby="flush-headingTwo"
              data-bs-parent="#patientsListFlush"
            >
              <div className="accordion-body">
                <ul className={`list-group ${styles.patientListGroup}`}>
                  {this.state.patientList?.map(
                    (patient: IPatient, idx: number) => {
                      return (
                        <li
                          className={`list-group-item ${styles.patientItem}`}
                          key={`${patient._id}`}
                        >
                          <div className="patient-card-body">
                            <h5 className="card-title">
                              {++idx}. {patient.firstName} {patient.lastName}
                            </h5>
                            <hr />
                            <small>Gender: {patient.gender}</small>
                            <br />
                            <hr style={{ maxWidth: "40px" }} />
                            <small>Description</small>
                            <p className="card-text">{patient.description}</p>
                            <a href="#" className="btn btn-primary disabled">
                              Show more information
                            </a>
                          </div>
                        </li>
                      );
                    }
                  )}
                </ul>
              </div>
            </div>
          </div>
        ) : null}
      </div>
    );
  }
}

export default PatientsCard;
