import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faArrowCircleRight,
  faExclamationCircle,
  IconDefinition,
} from "@fortawesome/free-solid-svg-icons";
import { NextPage } from "next";
import { withRouter } from "next/router";

import { getFormFields } from "../../shared/helper-methods.shared";
import { IField, WithRouterProps } from "../../shared/interfaces.shared";
import CustomLoader from "../custom-loader/Custom-loader.component";
import registrationPageStyles from "../../styles/Start.module.scss";
import styles from "./Card.module.scss";

interface Props extends WithRouterProps {
  borderTopColor?: string;
  cardTitle: string;
  description?: string;
  error?: boolean;
  errorMessage?: string;
  errorType?: number;
  formBody?: {
    imageSrc: string;
    imageAttr: string;
    formId: string;
    submitForm: Function;
    fields: IField[];
  };
  hrLine: boolean;
  icon?: IconDefinition;
  isForm?: boolean;
  loading: boolean;
  showCardImage?: boolean;
}

const CustomCard: NextPage<Props> = ({
  borderTopColor,
  cardTitle,
  description,
  error,
  errorMessage,
  errorType,
  formBody,
  hrLine,
  icon,
  isForm,
  loading,
  router,
  showCardImage = true,
}) => {
  const borderStyle =
    borderTopColor !== ""
      ? { borderTop: `10px solid ${borderTopColor}` }
      : { borderTop: "inherit" };

  const getCardBody = () => {
    if (isForm && formBody !== null && formBody.fields) {
      return (
        <div className={registrationPageStyles.cardBody}>
          <div className="row">
            {showCardImage ? (
              <div className="col-sm-6">
                <img
                  className={registrationPageStyles.registrationIllustration}
                  src={formBody.imageSrc}
                  alt={formBody.imageAttr}
                />
              </div>
            ) : null}
            <div className="col-sm-6">
              <code>{errorMessage}</code>
              <form
                className={registrationPageStyles.user_type_form}
                id={formBody.formId}
                onSubmit={(e) => formBody.submitForm(e, router)}
              >
                {formBody.fields.map((field: IField) => {
                  return getFormFields(undefined, field, formBody.formId);
                })}
                <button
                  className={`btn btn-primary ${registrationPageStyles.formButton}`}
                  type="submit"
                >
                  Continue
                  <FontAwesomeIcon
                    id={registrationPageStyles.formButtonArrow}
                    icon={faArrowCircleRight}
                  />
                </button>
              </form>
            </div>
          </div>
        </div>
      );
    } else {
      return <code>{description}</code>;
    }
  };

  // TODO: Set up different error styling.
  if (error) {
    return (
      <div
        className={styles.customErrorCard}
        style={
          (borderStyle.borderTop, { textAlign: isForm ? `left` : "center" })
        }
      >
        <div className={styles.errorIconDiv}>
          <FontAwesomeIcon
            className={styles.cardErrorIcon}
            icon={faExclamationCircle}
          />
        </div>
        <hr className={styles.submitLine} />
        <h4 className={styles.errorMessage}> {errorMessage}</h4>
      </div>
    );
  }

  return (
    <div
      className={styles.customCard}
      style={(borderStyle.borderTop, { textAlign: isForm ? `left` : "center" })}
    >
      {icon && <FontAwesomeIcon className={styles.cardIcon} icon={icon} />}
      <h3>{cardTitle}</h3>
      {hrLine && <hr className={styles.submitLine} />}
      {loading ? <CustomLoader /> : getCardBody()}
    </div>
  );
};

export default withRouter(CustomCard);
