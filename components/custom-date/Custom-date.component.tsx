import { IField } from "../../shared/interfaces.shared";
import styles from "../../styles/Start.module.scss";

const CustomDate = (field: IField) => {
  const fieldType = field.fieldType === "number" ? "number" : "text";
  return (
    <>
      <div className={`form-check ${styles.form_field}`} key={field.fieldID}>
        <label className="form-check-label">{field.fieldTitle}</label>
        <input className="form-control" type="date" id={field.fieldID} />
      </div>
    </>
  );
};

export default CustomDate;
