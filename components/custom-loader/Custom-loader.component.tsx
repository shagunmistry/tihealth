import styles from "./custom-loader.module.scss";

const CustomLoader = () => {
  return (
    <div className={styles.lds_ellipsis}>
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  );
};

export default CustomLoader;
