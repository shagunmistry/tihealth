import { ChangeEvent } from "react";
import { validateEmailAddress } from "../../shared/helper-methods.shared";
import { IField } from "../../shared/interfaces.shared";

interface IProps extends IField {
  formId: string;
}

const CustomInput = (props: IProps) => {
  const fieldType =
    props.fieldType === "number"
      ? "number"
      : props.fieldType === "password"
      ? "password"
      : "text";
  const validateField = (e: ChangeEvent) => {
    if (e.target.id === "userEmail") {
      const result = validateEmailAddress(e);
      if (!result) {
        document.getElementById(props.fieldID).classList.add("is-invalid");
      }
      if (result) {
        const classList = document.getElementById(props.fieldID).classList;
        classList.contains("is-invalid") ? classList.remove("is-invalid") : "";
      }
    }
  };
  return (
    <>
      <label className={props.fieldID}>{props.fieldTitle}</label>
      <input
        type={fieldType}
        className="form-control"
        id={props.fieldID}
        onChange={(e) => validateField(e)}
        required={props.required === undefined ? true : props.required}
        form={props.formId}
      />
    </>
  );
};

export default CustomInput;
