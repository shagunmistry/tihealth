import { Component } from "react";
import { withRouter } from "next/router";
import axios from "axios";
import cookies from "cookies";
import Head from "next/head";

import { getAuthCookies, resetAuthCookies } from "../middleware/utils";
import {
  GetProfileResponseType,
  UserFilesResponseType,
} from "../shared/types.shared";
import { IField, IPatient, WithRouterProps } from "../shared/interfaces.shared";
import { isUserLoggedIn } from "../lib/requireAuthentication";
import CustomLoader from "../components/custom-loader/Custom-loader.component";
import DocumentsCard from "../components/cards/DocumentsCard.component";
import ImageGallery from "../components/image-gallery/Image-gallery.component";
import PatientsCard from "../components/cards/PatientsCard.component";
import ProfileCard from "../components/cards/ProfileCard.component";
import styles from "../styles/Dashboard.module.scss";
import { IncomingMessage, ServerResponse } from "node:http";

interface MyComponentProps extends WithRouterProps {
  dataHasLoaded: boolean;
  documentsData: UserFilesResponseType;
  patientRegistrationForm: {
    title: string;
    formId: string;
    fields?: IField[];
  };
  token: string;
  userData: GetProfileResponseType;
}

interface MyComponentState {
  documentCardLoading: boolean;
}

/**
 * Dashboard components
 * - Profile information card
 *   - Name
 *   - Role on card
 *   - ID
 *   - birthday hidden (click to show)
 *   - email
 * - Documents
 * - Upcoming appointments
 * - personal notes section
 * As a patient:
 *  - Appointment scheduler
 *  - Doctor contact
 *  - health Info (entered by doctor).
 * As a doctor:
 * - Current patients
 * - Send msg to Patient
 */

class Dashboard extends Component<MyComponentProps, MyComponentState> {
  constructor(props) {
    super(props);

    this.state = {
      documentCardLoading: false,
    };
  }

  componentDidMount() {
    if (this.props.userData === null || this.props.userData === undefined) {
      this.props.router.push("/login");
    }
  }

  refreshData = () => {
    this.props.router.replace(this.props.router.asPath);
  };

  /**
   * Handle file upload for the Documents Card.
   * @param body
   */
  handleFileUpload = async (body: any) => {
    try {
      this.setState({ documentCardLoading: true });
      const formBody = new FormData();

      formBody.append("description", body.description);
      formBody.append("file", body.file);
      formBody.append("for", body.for);
      formBody.append("name", body.name);
      formBody.append("uploadBy", this.props.userData._id);
      formBody.append("uploadTime", body.uploadTime);

      const res = await axios
        .post(`${process.env.TIHEALTH_API_URL}fileupload/upload`, formBody, {
          headers: {
            Authorization: `Bearer ${this.props.token}`,
            "Content-Type": "multipart/form-data",
          },
        })
        .catch((e: any) => {
          throw new Error(e.message);
        });

      if (res && res.data && res.status >= 200 && res.status < 400) {
        this.setState({ documentCardLoading: false });
        this.refreshData();
      }
    } catch (e: any) {
      // TODO: Add error notification here:
      console.log("There was an error uploading files: ", e.message);
      this.setState({ documentCardLoading: false });
    }
  };

  logOut = () => {
    resetAuthCookies();
    this.props.router.push("/login");
  };

  getPatientNameListForDocumentUpload = async (): Promise<
    Map<string, string>
  > => {
    const patientIdAndNames = new Map<string, string>();

    try {
      const res = await axios.get(
        `api/patient?doctorId=${this.props.userData?._id}`
      );
      if (res && res.data && res.status === 200 && res.data.length) {
        res.data.forEach((patient: IPatient) => {
          patientIdAndNames.set(
            patient._id,
            `${patient.firstName} ${patient.lastName}`
          );
        });
      }
    } catch (e: any) {
      console.log("There was an error getting patient list: ", e);
    }

    return patientIdAndNames;
  };

  render() {
    const { userData } = this.props;

    if (!this.props.dataHasLoaded) {
      return <CustomLoader></CustomLoader>;
    }

    return (
      <div
        className={`container ${styles.dashboardContainer}`}
        style={{ marginRight: "auto", marginLeft: "auto" }}
      >
        <Head>
          <title>
            {userData.firstName} {userData.lastName} Dashboard
          </title>
        </Head>

        <div>
          <button
            type="button"
            className={`btn btn-outline-danger ${styles.logoutButton}`}
            onClick={() => this.logOut()}
          >
            Logout
          </button>
          <h2 style={{ textAlign: "center" }}>
            {userData.firstName} {userData.lastName} Dashboard
          </h2>
          <div className="row row-cols-1 row-cols-md-2 g-4">
            <div className={`col ${styles.eachCardCol}`}>
              <ProfileCard
                FirstName={userData.firstName}
                id={userData._id}
                LastName={userData.lastName}
                roleType={userData.roles ? userData.roles[0] : ""}
                userBirthday={new Date(userData.userBirthday)}
                userEmail={userData.userEmail}
              />
            </div>
            <div className={`col ${styles.eachCardCol}`}>
              <DocumentsCard
                {...this.props.documentsData}
                uploadFiles={this.handleFileUpload}
                loading={this.state.documentCardLoading}
                patients={this.getPatientNameListForDocumentUpload()}
              />
            </div>
          </div>
          <hr />
          <PatientsCard doctorId={this.props.userData._id} />
          <h2>Files</h2>
          <ImageGallery
            {...this.props.documentsData}
            patients={this.getPatientNameListForDocumentUpload()}
          />
        </div>
      </div>
    );
  }
}

export const getServerSideProps = async (context: {
  req: IncomingMessage;
  res: ServerResponse;
}) => {
  const currentCookieInstance = new cookies(context.req, context.res);
  const { userToken: token, userEmail } = getAuthCookies(currentCookieInstance);

  let dataHasLoaded = false;
  let documentsData: UserFilesResponseType = null;
  let patientList = [];
  let patientRegistrationForm = null;
  let userData: GetProfileResponseType = null;

  if (token && userEmail) {
    try {
      userData = await isUserLoggedIn(token, userEmail);
    } catch (e: any) {
      console.log(
        "DashboardPage --> getServerSideProps error getting profile info data: ",
        e.message
      );
    }

    try {
      if (userData._id) {
        const res = await axios.get(
          `${process.env.TIHEALTH_API_URL}fileupload/files/${userData._id}?method=uploadBy`,
          {
            headers: {
              Authorization: `Bearer ${token}`,
            },
          }
        );

        documentsData = res.data;

        const patientRegistrationRes = await axios.get(
          `${process.env.TIHEALTH_API_URL}registration/form/2?userType=patient`
        );

        patientRegistrationForm =
          patientRegistrationRes && patientRegistrationRes.data
            ? patientRegistrationRes.data
            : null;
      }
    } catch (e: any) {
      console.log(
        "DashboardPage --> getServerSideProps error getting document data: ",
        e.message
      );
    }
    dataHasLoaded = userData._id ? true : false;
  }

  return {
    props: {
      dataHasLoaded,
      documentsData,
      patientRegistrationForm,
      token,
      userData,
    },
  };
};

export default withRouter(Dashboard);
