import axios from "axios";
import type { NextApiRequest, NextApiResponse } from "next";
import Cookies from "cookies";
import { JWT_TOKEN_CONST, USER_EMAIL_CONST } from "../../shared/constants";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  if (req.method?.toLowerCase() === "post") {
    // Create a cookies instance
    const cookies = new Cookies(req, res);

    // This is a POST request to /api/user to login the user and get the token if valid
    // if there's a token, set the cookie to httpOny with the JWT token
    // once that is done, redirect the user to /dashboard.
    try {
      const apiRes = await axios.post(
        `${process.env.TIHEALTH_API_URL}auth/login`,
        JSON.stringify(req.body),
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );

      if (apiRes && apiRes.data && apiRes.data.access_token) {
        cookies.set(JWT_TOKEN_CONST, apiRes.data.access_token, {
          httpOnly: process.env.NODE_ENV === "production",
        });
        cookies.set(USER_EMAIL_CONST, req.body.userEmail, {
          httpOnly: process.env.NODE_ENV === "production",
        });
      }

      res
        .status(200)
        .json({ message: "You have been successfully logged in!" });
    } catch (e: any) {
      res
        .status(401)
        .json({ message: `There was an error:  ${JSON.stringify(e)}` });
    }
  }
}
