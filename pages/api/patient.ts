import axios from "axios";
import type { NextApiRequest, NextApiResponse } from "next";
import Cookies from "cookies";
import { JWT_TOKEN_CONST, USER_EMAIL_CONST } from "../../shared/constants";
import { getAuthCookies } from "../../middleware/utils";

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse<any>
) {
  if (req.method?.toLowerCase() === "post") {
    // Create a cookies instance
    const cookies = new Cookies(req, res);
    const { userToken: token } = getAuthCookies(cookies);

    try {
      const apiRes = await axios.post(
        `${process.env.TIHEALTH_API_URL}patient`,
        JSON.stringify(req.body),
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      );

      if (apiRes && apiRes.data) {
        res.status(200).json(apiRes.data);
      } else {
        throw new Error(
          "There was an error submitting a new patient: " + apiRes.status
        );
      }
    } catch (e: any) {
      console.log("There was an error: ", e);
      res
        .status(400)
        .json({ message: `There was an error:  ${JSON.stringify(e)}` });
    }
  }

  if (req.method?.toLowerCase() === "get") {
    const cookies = new Cookies(req, res);
    const { userToken: token } = getAuthCookies(cookies);

    console.log("Request query: ", req.query);
    try {
      const apiRes = await axios.get(
        `${process.env.TIHEALTH_API_URL}patient/doctor/${req.query.doctorId}`,
        {
          headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${token}`,
          },
        }
      );

      if (apiRes && apiRes.data) {
        res.status(200).json(apiRes.data);
      } else {
        throw new Error(
          "There was an error getting PATIENT LIST: " + apiRes.status
        );
      }
    } catch (e: any) {
      console.log("There was an error: ", e);
      res.status(400).json({
        message: `There was an error getting patient data:  ${JSON.stringify(
          e
        )}`,
      });
    }
  }
}
