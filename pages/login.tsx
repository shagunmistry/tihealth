import { GetStaticProps, NextPage } from "next";
import { useState } from "react";
import { withRouter } from "next/router";
import axios from "axios";
import Head from "next/head";
import Link from "next/link";

import { IField, WithRouterProps } from "../shared/interfaces.shared";
import { validateField } from "../shared/helper-methods.shared";
import CustomCard from "../components/cards/card.component";

interface MyComponentProps extends WithRouterProps {
  authenticated: boolean;
  data: {
    fields?: IField[];
    formId: string;
    title: string;
  };
}

const Login: NextPage<MyComponentProps> = ({ authenticated, data, router }) => {
  const [error, setError] = useState("");
  const [loading, setLoading] = useState(false);

  if (authenticated) {
    router.push("/dashboard");
  }

  const submitForm = async (e: any) => {
    e.preventDefault();
    setLoading(true);
    setError("");

    if (e.target && e.target.length > 0) {
      let submission: any = {};
      Array.from(e.target).forEach((target: any) => {
        if (target && target.id && target.id !== "") {
          const isValidate = validateField(target.id, target.value);

          if (isValidate) {
            if (target.checked) {
              // This is for the radio button for deciding user type.
              submission.userType = target.value;
            }
            submission[target.id] = target.value;
          }
        }
      });

      try {
        const res = await axios.post(`api/user`, submission);
        if (res.status === 200) {
          router.push("/dashboard");
        }
      } catch (e: any) {
        setLoading(false);
        console.log("E: ", e.status);
        setError(`There was wdawawd error: ${e.message}`);
      }
    }
  };
  return (
    <>
      <Head>
        <title>TiHealth Login</title>
      </Head>
      <br />
      <p style={{ textAlign: "center" }}>
        If you do not have an account, please{" "}
        <i>
          <Link href="/registration">
            <a style={{ color: "blue", textDecoration: "underline" }}>
              register here.
            </a>
          </Link>
        </i>
      </p>
      <CustomCard
        cardTitle={data.title}
        hrLine={false}
        formBody={{
          fields: data.fields,
          formId: data.formId,
          imageAttr: "Login Illustration Pic",
          imageSrc:
            "https://firebasestorage.googleapis.com/v0/b/queen-x.appspot.com/o/TiHealth%2Fhealth-illustrations_svg%2Fremote-work-man.svg?alt=media&token=be18724a-525d-40c3-8214-ce42848baefb",
          submitForm,
        }}
        errorMessage={error}
        isForm={true}
        loading={loading}
      />
    </>
  );
};

export const getStaticProps: GetStaticProps = async (context) => {
  const res = await fetch(`${process.env.TIHEALTH_API_URL}auth/login/form`);
  const data = await res.json();

  if (!data) {
    return {
      props: { authenticated: false, notFound: true },
    };
  }

  return {
    props: { authenticated: false, data },
  };
};

export default withRouter(Login);
