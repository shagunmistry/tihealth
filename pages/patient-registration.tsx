import { withRouter } from "next/router";
import Head from "next/head";
import React from "react";

import { IField, WithRouterProps } from "../shared/interfaces.shared";
import CustomCard from "../components/cards/card.component";
import CustomLoader from "../components/custom-loader/Custom-loader.component";

interface PatientRegistrationState {
  error: boolean;
  errorMessage: string;
  errorType: number;
  fields?: IField[];
  formId: string;
  loading: boolean;
  title: string;
}

class PatientRegistration extends React.Component<
  WithRouterProps,
  PatientRegistrationState
> {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      errorMessage: "",
      errorType: null,
      fields: null,
      formId: null,
      loading: true,
      title: null,
    };
  }

  async componentDidMount() {
    const userType = this.props.router.query.userType;
    const userId = this.props.router.query.userId;
    const res = await fetch(
      `${process.env.TIHEALTH_API_URL}registration/form/2?userType=${userType}&userId=${userId}`
    );
    const data = await res.json();
    if (res && res.ok && data) {
      this.setState({
        fields: data.fields,
        formId: data.formId,
        title: `Welcome, ${data.firstName || "there!"} | ${data.title}`,
        loading: false,
      });
    } else {
      this.setState({
        errorType: res.status,
        error: true,
        errorMessage:
          "There was an error processing the form, please try again. Error details: " +
          data.message,
        loading: false,
      });
    }
  }

  submitPatientRegistration = (e: any) => {
    e.preventDefault();

    let submission: any = {};
    Array.from(e.target).forEach((target: any) => {
      const fieldByTargetId = this.state.fields.find(
        (e: IField) => e.fieldID === target.id
      );

      const documentById = fieldByTargetId
        ? document.getElementById(fieldByTargetId?.fieldID)
        : document.getElementById(target.id);

      const val = documentById ? (documentById as HTMLInputElement) : null;
      // Check if its a dropdown
      if (val && val.classList.toString().includes("dropdown")) {
        submission[target.id] = val.innerText;
      } else if (target.id !== "" || target.id !== undefined) {
        submission[target.id] = target.value;
      }
    });

    submission["userId"] = this.props.router.query.userId;
    submission["userType"] = this.props.router.query.userType?.toString();
  };

  render() {
    return (
      <>
        <Head>
          <title>TiHealth Patient Registration</title>
        </Head>
        {this.state.fields === undefined ? (
          <div className="container">
            <CustomLoader />
          </div>
        ) : (
          <CustomCard
            cardTitle={this.state.title}
            hrLine={false}
            formBody={{
              fields: this.state.fields,
              formId: this.state.formId,
              imageAttr: "Patient Registration Illustration Pic",
              imageSrc:
                "https://firebasestorage.googleapis.com/v0/b/queen-x.appspot.com/o/TiHealth%2Fhealth-illustrations_svg%2Fremote-work-man.svg?alt=media&token=be18724a-525d-40c3-8214-ce42848baefb",
              submitForm: (e) => {
                this.submitPatientRegistration(e);
              },
            }}
            isForm={true}
            error={this.state.error}
            errorMessage={this.state.errorMessage}
            errorType={this.state.errorType}
            loading={this.state.loading}
          />
        )}
      </>
    );
  }
}

export default withRouter(PatientRegistration);
