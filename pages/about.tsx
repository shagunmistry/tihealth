import Head from "next/head";
import { withRouter } from "next/router";

import styles from "../styles/About.module.scss";

const About = () => {
  return (
    <>
      <Head>
        <title>About TiHealth</title>
      </Head>
      <div className="container">
        <div className={`${styles.about}`}></div>
        <div className={`${styles.aboutContainer}`}>
          <h1 className={`${styles.pageHeader}`}>About Us</h1>
          <p>
            TiHealth's mission is to provide a direct bridge between patients
            and their healthcare professionals. We aim to make the relationship
            between a patient and their healthcare professional as much
            transparent as possible.{" "}
          </p>
          <p>
            We are a team of extremely dedicated people, who work mainly as
            doctors, engineers, and healthcare workers.
          </p>
          <hr className={`${styles.aboutHrLine}`} />
          <br />

          {process.env.NODE_ENV !== "development" ? null : (
            <span className={`${styles.ourTeamContainer}`}>
              <h1 className={`${styles.pageHeader}`}>Our Team</h1>
              <div className="row">
                <div className="col">
                  <div className={`card ${styles.contactCard}`}>
                    <img
                      className={`card-img-top ${styles.contactCardImage}`}
                      src="https://static.theceomagazine.net/wp-content/uploads/2018/10/15093202/elon-musk.jpg"
                      alt="Card image cap"
                    />
                    <div className={`card-body ${styles.contactCardBody}`}>
                      <h3 className={`${styles.contactCardTitle}`}>
                        Firstname Lastname
                      </h3>
                      <p className="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className={`card ${styles.contactCard}`}>
                    <img
                      className={`card-img-top ${styles.contactCardImage}`}
                      src="https://static.theceomagazine.net/wp-content/uploads/2018/10/15093202/elon-musk.jpg"
                      alt="Card image cap"
                    />
                    <div className={`card-body ${styles.contactCardBody}`}>
                      <h3 className={`${styles.contactCardTitle}`}>
                        Firstname Lastname
                      </h3>
                      <p className="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                    </div>
                  </div>
                </div>
                <div className="col">
                  <div className={`card ${styles.contactCard}`}>
                    <img
                      className={`card-img-top ${styles.contactCardImage}`}
                      src="https://static.theceomagazine.net/wp-content/uploads/2018/10/15093202/elon-musk.jpg"
                      alt="Card image cap"
                    />
                    <div className={`card-body ${styles.contactCardBody}`}>
                      <h3 className={`${styles.contactCardTitle}`}>
                        Firstname Lastname
                      </h3>
                      <p className="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </span>
          )}
        </div>
      </div>
    </>
  );
};

export default withRouter(About);
