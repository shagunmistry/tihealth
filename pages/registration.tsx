import { NextPage } from "next";
import { NextRouter, withRouter } from "next/router";
import { useState } from "react";
import axios from "axios";
import Head from "next/head";
import Link from "next/link";

import { IField, WithRouterProps } from "../shared/interfaces.shared";
import { validateField } from "../shared/helper-methods.shared";
import CustomCard from "../components/cards/card.component";

interface MyComponentProps extends WithRouterProps {
  data: {
    title: string;
    formId: string;
    fields?: IField[];
  };
}

const Registration: NextPage<MyComponentProps> = ({ data, router }) => {
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState("");

  /**
   * Submit registration form
   * @param e
   */
  const submitForm = async (e: any, router: NextRouter) => {
    e.preventDefault();
    setLoading(true);

    if (e.target && e.target.length > 0) {
      let submission: any = {};
      Array.from(e.target).forEach((target: any) => {
        if (target && target.id && target.id !== "") {
          const isValidate = validateField(target.id, target.value);

          if (isValidate) {
            if (target.checked) {
              // This is for the radio button for deciding user type.
              submission.userType = target.value;
            }
            submission[target.id] = target.value;
          }
        }
      });

      if (submission.userType === undefined) {
        setError(
          "Please select either Patient or Healthcare worker so that we can better serve you."
        );
        setLoading(false);
      } else {
        const res = await axios.post(
          `${process.env.TIHEALTH_API_URL}registration`,
          submission
        );
        if (res && res.data && res.data.error) {
          // TODO: Update registration error handling
          setLoading(false);
          setError(
            `There was an error with submitting data: ${res.data.message}`
          );
        } else if (res && res.data && !res.data.error) {
          setLoading(false);
          router.push("/login");
        }
      }
    }
  };

  return (
    <>
      <Head>
        <title>TiHealth Registration</title>
      </Head>
      <br />
      <p style={{ textAlign: "center" }}>
        If you already have an account, please{" "}
        <i>
          <Link href="/login">
            <a style={{ color: "blue", textDecoration: "underline" }}>
              sign in here.
            </a>
          </Link>
        </i>
      </p>
      <CustomCard
        cardTitle={data.title}
        hrLine={false}
        formBody={{
          fields: data.fields,
          formId: data.formId,
          imageAttr: "Registration Illustration Pic",
          imageSrc:
            "https://firebasestorage.googleapis.com/v0/b/queen-x.appspot.com/o/TiHealth%2Fhealth-illustrations_svg%2Fremote-work-woman.svg?alt=media&token=c6cbbc39-a21a-4967-af82-63fd2713b731",
          submitForm,
        }}
        errorMessage={error}
        isForm={true}
        loading={loading}
      />
    </>
  );
};

export async function getStaticProps() {
  console.log("process.env.TIHEALTH_API_URL: ", process.env.TIHEALTH_API_URL);
  const res = await fetch(`${process.env.TIHEALTH_API_URL}registration/form/1`);
  const data = await res.json();

  if (!data) {
    return {
      notFound: true,
    };
  }

  return {
    props: { data },
  };
}

export default withRouter(Registration);
