import Head from "next/head";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowCircleRight } from "@fortawesome/free-solid-svg-icons";

import { homePageCardData } from "../shared/constants";
import CustomCard from "../components/cards/card.component";
import styles from "../styles/Home.module.scss";

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>TiHealth</title>
        <link rel="icon" href="/favicon.ico" />
        <link
          href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/css/bootstrap.min.css"
          rel="stylesheet"
          integrity="sha384-wEmeIV1mKuiNpC+IOBjI7aAzPcEZeedi5yW5f2yOq55WWLwNGmvvx4Um1vskeMj0"
          crossOrigin="anonymous"
        />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>Welcome to TiHealth</h1>
        <p className={styles.description}>Where your health is a priority</p>
        <hr className={styles.submitLine} />
        <div className={styles.submitCard}>
          <button
            className={`btn btn-lg btn-primary ${styles.submitButton}`}
            onClick={() => {
              window.location.assign("/registration");
            }}
          >
            Go To Dashboard{" "}
            <FontAwesomeIcon
              id={styles.submitButtonArrow}
              icon={faArrowCircleRight}
            />
          </button>
        </div>
        <hr className={styles.submitLine} />
        <div className={styles.grid}>
          {homePageCardData.map((eachCard) => {
            return (
              <CustomCard
                cardTitle={eachCard.cardTitle}
                description={eachCard.description}
                hrLine={eachCard.hrLine}
                icon={eachCard.icon}
                key={eachCard.cardTitle.trim().replace(" ", "")}
                loading={false}
              />
            );
          })}
        </div>
      </main>
    </div>
  );
}
