import "nprogress/nprogress.css";
import NProgress from "nprogress";
import Router from "next/router";

import "../styles/globals.scss";
import CustomFooter from "../components/footer/Footer.component";
import Navbar from "../components/navbar/Navbar.component";
import styles from "../styles/App.module.scss";
import { AppProps } from "next/dist/next-server/lib/router/router";

//Binding events.
Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <div className={styles.pageContainer}>
      <div className={styles.contentWrap}>
        <Navbar />
        <Component {...pageProps} />
      </div>

      <CustomFooter />
      <script
        src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossOrigin="anonymous"
      ></script>
      <script
        src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q"
        crossOrigin="anonymous"
      ></script>
      <script
        src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-p34f1UUtsS3wqzfto5wAAmdvj+osOnFyQFpp4Ua3gs/ZVWx6oOypYoCJhGGScy+8"
        crossOrigin="anonymous"
      ></script>
    </div>
  );
}

export default MyApp;
